/**
 * 1. Write a method(s) that checks whether the given three numbers are coprime.
 */
public class Task1 {

    public static void main(String[] args) {
        System.out.println("Numbers are co-prime?" +
                "\n\t5, 12, 18 - " + checkNumbersAreCoPrime(5, 12, 18) +
                "\n\t1, 6, 12 - " + checkNumbersAreCoPrime(5, 12, 18) +
                "\n\t10, 100, 200 - " + checkNumbersAreCoPrime(10, 100, 200));

    }

    private static boolean checkNumbersAreCoPrime(int firstNumber, int secondNumber, int thirdNumber) {
        return findGreatestCommonDivisor(firstNumber, secondNumber, thirdNumber) == 1;
    }

    private static int findGreatestCommonDivisor(int firstNumber, int secondNumber) {
        int tempFirst = firstNumber;
        int tempSecond = secondNumber;

        while (tempFirst != 0 && tempSecond != 0) {
            if (tempFirst > tempSecond) {
                tempFirst = tempFirst % tempSecond;
            } else {
                tempSecond = tempSecond % tempFirst;
            }
        }

        return tempFirst + tempSecond;
    }

    private static int findGreatestCommonDivisor(int... numbers) {
        if (numbers.length < 2) {
            return 0;
        }

        int gcd = findGreatestCommonDivisor(numbers[0], numbers[1]);
        for (int i = 2; i < numbers.length; i++) {
            gcd = findGreatestCommonDivisor(gcd, numbers[i]);
        }

        return gcd;
    }
}
