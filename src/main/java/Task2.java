/**
 * 2. Implement the addition of two numbers of unlimited length. Use the String type to store numbers.
 * To solve the problem, use the division of code into methods.
 */
public class Task2 {

    public static void main(String[] args) {
        String num1 = "123456789123456789123456789123456789";
        String num2 = "987654321987654321987654321987654321987654321";
        String sum = add(num1, num2);
        System.out.println(num1 + " + " + num2 + " = " + sum);
    }

    private static String add(String firstNumber, String secondNumber) {
        int maxLength = Math.max(firstNumber.length(), secondNumber.length());
        firstNumber = addLeadingZeros(firstNumber, maxLength - firstNumber.length());
        secondNumber = addLeadingZeros(secondNumber, maxLength - secondNumber.length());

        StringBuilder result = new StringBuilder();
        int carry = 0;
        for (int i = maxLength - 1; i >= 0; i--) {
             int sum = calculateSumOfChars(firstNumber.charAt(i), secondNumber.charAt(i)) + carry;
             result.append(sum % 10);
             carry = sum / 10;
        }

        if (carry > 0) {
            result.append(carry);
        }

        return result.reverse().toString();
    }

    private static String addLeadingZeros(String number, int zerosToAdd) {
        if (zerosToAdd <= 0) {
            return number;
        }

        return String.format("%0" + zerosToAdd + "d%s", 0, number);
    }

    private static int calculateSumOfChars(char firstChar, char secondChar) {
        // need to subtract '0' from a char to get integer
        int firstDigit = firstChar - '0';
        int secondDigit = secondChar - '0';
        return firstDigit + secondDigit;
    }
}
