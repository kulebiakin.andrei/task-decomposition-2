import java.util.Arrays;

/**
 * 3. A natural number with n digits is called an Armstrong number if the sum of its digits raised
 * to the power of n is equal to the number itself. Find all Armstrong numbers from 1 to k.
 * To solve the problem, use the division of code into methods.
 */
public class Task3 {

    public static void main(String[] args) {
        int start = 1;
        int end = 1_000;
        int[] armstrongNumbers = findArmstrongNumbers(start, end);

        System.out.printf("Armstrong numbers from %d to %d\n", start, end);
        for (int number : armstrongNumbers) {
            System.out.println(number);
        }
    }

    private static int[] findArmstrongNumbers(int start, int end) {
        int[] result = new int[end - start + 1];
        int resultIndex = 0;

        for (int i = start; i <= end; i++) {
            if (isArmstrongNumber(i)) {
                result[resultIndex] = i;
                resultIndex++;
            }
        }

        return Arrays.copyOf(result, resultIndex);
    }

    private static boolean isArmstrongNumber(int number) {
        int[] digits = splitNumberToDigits(number);
        int numberOfDigits = digits.length;

        int sumOfPowers = 0;
        for (int digit : digits) {
            sumOfPowers = sumOfPowers + calculatePower(digit, numberOfDigits);
        }

        return sumOfPowers == number;
    }

    private static int[] splitNumberToDigits(int number) {
        char[] digitsAsChars = String.valueOf(number).toCharArray();
        int[] result = new int[digitsAsChars.length];

        for (int i = 0; i < result.length; i++) {
            result[i] = digitsAsChars[i] - '0';
        }

        return result;
    }

    private static int calculatePower(int number, int power) {
        return (int) Math.pow(number, power);
    }
}
