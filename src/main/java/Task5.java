import java.util.Arrays;

/**
 * 5. «Super Lock». The secret lock for the safe consists of 10 cells arranged in a row, into which you need to insert dice.
 * But the door opens only if in any three neighboring cells the sum of points on the front faces of the dice is 10.
 * (A dice has from 1 to 6 points on each face). Write a program that solves the code of the lock, provided that
 * two dice are already inserted into the cells. To solve the problem, use the division of code into methods.
 */
public class Task5 {

    public static void main(String[] args) {
        int requiredNumberOfNeighborElements = 3;
        int requiredSumOfNeighborElements = 10;

        // The first (requiredNumberOfNeighborElements - 1) elements need to be given
        // For example, if requiredNumberOfNeighborElements = 3, then first two elements need to be filled
        int[] lock = {2, 3, 0, 0, 0, 0, 0, 0, 0, 0};

        System.out.println("Given lock: " + Arrays.toString(lock));

        solveTheLock(lock, requiredNumberOfNeighborElements, requiredSumOfNeighborElements);

        System.out.println("Solved lock: " + Arrays.toString(lock));
        System.out.println("The door is open: " +
                isLockOpened(lock, requiredNumberOfNeighborElements, requiredSumOfNeighborElements));
    }

    private static void solveTheLock(int[] lock, int requiredNumberOfNeighborElements, int requiredSumOfNeighborElements) {
        // first elements except one are given
        int numberOfGivenElements = requiredNumberOfNeighborElements - 1;

        for (int i = numberOfGivenElements; i < lock.length; i++) {
            int givenSum = Task4.calculateSumOfElements(lock, i - numberOfGivenElements, numberOfGivenElements);
            lock[i] = calculateNextDice(givenSum, requiredSumOfNeighborElements);
        }
    }

    private static int calculateNextDice(int givenSum, int requiredSumOfElements) {
        return requiredSumOfElements - givenSum;
    }

    /**
     * The lock is open only if three neighboring cells sum is 10 across the lock
     */
    private static boolean isLockOpened(int[] lock, int requiredNumberOfNeighborElements, int requiredSumOfNeighborElements) {
        for (int i = 0; i < lock.length - requiredNumberOfNeighborElements; i++) {
            int sumOfConsecutiveElements = Task4.calculateSumOfElements(lock, i, requiredNumberOfNeighborElements);
            if (sumOfConsecutiveElements != requiredSumOfNeighborElements) {
                return false;
            }
        }
        return true;
    }
}
