import java.util.Arrays;

/**
 * 4. An array D is given. Determine the following sums: D[l] + D[2] + D[3]; D[3] + D[4] + D[5]; D[4] +D[5] +D[6].
 * Explanation. Compose a method(s) for calculating the sum of three consecutive array elements with numbers from k to m.
 */
public class Task4 {

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        System.out.println("Given array: " + Arrays.toString(array));
        System.out.println("D[0] + D[1] + D[2] = " + calculateSumOfElementsBetweenIndexes(array, 0, 2));
        System.out.println("D[1] + D[2] + D[3] = " + calculateSumOfElementsBetweenIndexes(array, 1, 3));
        System.out.println("D[3] + D[4] + D[5] = " + calculateSumOfElementsBetweenIndexes(array, 3, 5));
        System.out.println("D[4] + D[5] + D[6] = " + calculateSumOfElementsBetweenIndexes(array, 4, 6));
    }

    public static int calculateSumOfElementsBetweenIndexes(int[] array, int startIndex, int endIndex) {
        int numberOfElements = endIndex - startIndex + 1;
        return calculateSumOfElements(array, startIndex, numberOfElements);
    }

    public static int calculateSumOfElements(int[] array, int start, int numberOfElements) {
        int sum = 0;

        int end = start + numberOfElements;
        for (int i = start; i < end; i++) {
            sum = sum + array[i];
        }

        return sum;
    }
}
